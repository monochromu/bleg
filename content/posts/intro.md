+++
title = "Intro"
author = ["Mono"]
date = 2021-01-20T22:09:00+05:30
lastmod = 2021-03-05T14:45:52+05:30
draft = false
weight = 2001
+++

## . {#dot}

![](~/Developments/.monochromu/bleg/static/scary.gif)
Welcome to this small corner on the end, I am your host _Mono_, I'm glad that you are here, and chances are that you have come here because someone sent you this.
That's great, 99% that will be the person who's writing this. This is just to have a safe place on the internet, that captures some of the memories we have shared, and probably me making up
random crap.

Anyway I hope, that in future people look at this like some historical fact and wonder what the fuck did we mean by all this. Like this [actually](https://futurama.fandom.com/wiki/Head%5FMuseum)

Peace,

M

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
