+++
title = "My OCD roommate?"
author = ["Mono"]
date = 2021-01-22T00:00:00+05:30
lastmod = 2021-01-23T09:24:22+05:30
draft = true
weight = 2001
+++

<div class="ox-hugo-toc toc">
<div></div>

<div class="heading">Table of Contents</div>

- [OK, weird flex.](#ok-weird-flex-dot)
    - [Do you like listening to music while reading? I kinda of do.](#do-you-like-listening-to-music-while-reading-i-kinda-of-do-dot)
- [When?](#when)
- [~~Eccentricities~~ ; How to be PITA](#how-to-be-pita)
    - [Switch Fight!](#switch-fight)
    - [OCD](#ocd)

</div>
<!--endtoc-->

{{< figure src="https://upload.wikimedia.org/wikipedia/commons/3/33/Parental%5FAdvisory%5Flabel.svg" >}}


## OK, weird flex. {#ok-weird-flex-dot}

What's this about? Like I had mentioned this is a blog focused on "rants!". This guy is a piece of work. My god!
Audacity of this asshole. Anyway let me start from the beginning. So, we all kinda of stay in one room, and all of us are in a hostel. So, yeah you could just get the fuck out without worrying a lot.
We were 4 people in a room, perks of being in a 3rd world country, heh.


### Do you like listening to music while reading? I kinda of do. {#do-you-like-listening-to-music-while-reading-i-kinda-of-do-dot}

Some of the tracks I recommend for reading this rant:

-   [2Pac - Hit 'Em Up](https://www.youtube.com/watch?v=41qC3w3UUkU)
-   [The Real Slim Shady](https://music.youtube.com/watch?v=r5MR7%5FINQwg)


## When? {#when}

This guy enters my life, almost 5 years ago. Fresh out of home, coming into a new place, I see this guy in my room, with my other roommates standing outside. He kinda of came first so took the most ideal location for his bed, took the best cupboard, already had his wings spread around the place, it felt a bit unfair TBH but whatever first person to reach usually calls the dibs right!


## ~~Eccentricities~~ ; How to be PITA {#how-to-be-pita}

So, fast fowarding the settlement part, he seemed to have a strict sleep schedule. He used to get into bed at 11 or so, which was fine, I mean personal freedom, but they weird part was turning the fucking light off. I mean he couldn't sleep with out it. Strange right? So we thought that if majority was going it made sense to turn of the lights and let the person work outside or in another room.
So, that is if 2 people or 3 of them were going to sleep, we'd turn the lights off. Seeing movies without lights might give good feels for horror movies, but generally turns out to be very bad for your eyes.


### Switch Fight! {#switch-fight}

But this magnificent piece of shit, couldn't really sleep even if he was the only guy who was going to sleep, he'd still fucking turn of the lights. At a point, I got into a huge switch fight, just to fucking prove a point. None of my roommates where epileptic, otherwise it would really have been a bad day for them.

Wait, you'd still be wondering what the fuck is a switch fight right?
Basically, this is just him turning off the lights, and me turning them back on. What a nice way to spend your evening. Yeah! Just was hoping that the switch doesn't stop working after that.


### OCD {#ocd}

Yeah this guy has this problem, you'd have heard about OCD right? Obsessive Compulsive Disorder. He just had an unhealthy obsession for cleanliness, difficult to maintain in a third world country, but yeah good to always have standards. The issue was not him having this compulsiveness ; but for a weird idea attached to that, which was to be clean that you had to make sure others aren't.
What a standard bitch!

That being said he actually reminds me of this guy!

{{< figure src="https://s01.sgp1.digitaloceanspaces.com/facebook/886869-wtneijhdws-1531854580.jpeg" >}}

Seems like that right?
Kinda of like that eventhough not exactly similar.

So, he had this weird idea about others failing and he standing at the top, couldn't never really think about _win-win_ situation, always I _win-(you)-lose_ attitude, we tried changing that, but no chance, he didn't really wanna change, knowing that you are being called a dick, still OK with being a dick. That requires more elephant-thick skin.

And yeah had zero _cojones_, I mean you should see him freak-out too easily.

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
